import { nanoid } from 'nanoid';

// HEAD DATA
export const headData = {
  title: 'Prabhjot Dev', // e.g: 'Name | Developer'
  lang: '', // e.g: en, es, fr, jp
  description: 'Welcome to my website ', // e.g: Welcome to my website
};

// HERO DATA
export const heroData = {
  title: '',
  name: 'Prabhjot Dev',
  subtitle: 'Im a Software Developer.',
  cta: '',
};

// ABOUT DATA
export const aboutData = {
  img: 'profile.jpg',
  paragraphOne:
    'A dedicated and talented Software Developer with 1 year of professional experience.',
  paragraphTwo:
    'I have experience in Full Stack development earned at diverse companies such as, Government of Ontario, and SIRT, where I was exposed to different technology stacks and practices. I am skilled in understanding business needs, specifying requirements, and implementing software with the highest standards in mind.',
  paragraphThree:
    'I have used Java and C# extensively however lately I have developed an interest for React. I am very passionate and deeply curious about Computer Science and Software Engineering which is why I am continuing my education at Sheridan College. Currently, my interests are software engineering, cross platform development, and artificial intelligence (specifically machine learning).',
  paragraphFour:
    'I am always on the lookout for challenging opportunities and currently open to hiring.',
  resume: 'https://drive.google.com/file/d/1-mJthYV8CsNW5X0-MRsgDbjy7D1L9gmN/view?usp=sharing', // 'https://www.resumemaker.online/es.php', // if no resume, the button will not show up
};

// PROJECTS DATA
export const projectsData = [
  {
    id: nanoid(),
    img: 'Recruitment_Small.jpg',
    title: 'Recruitment',
    info:
      'The website was built using the basic knowledege of HTML, CSS, and Javascript to demonstrate a simple webstite that asks/saves information from user and displays the result below the form.',
    info2: '',
    url: '',
    repo: 'https://github.com/prabhjotdev/Recrutment', // if no repo, the button will not show up
  },
  {
    id: nanoid(),
    img: 'Meal.ly_Home.jpg',
    title: 'Meal.ly',
    info:
      "A book with different recipes. It explains in detail how to prep an selected food. You can also filter out which ones you don't want. And add food to your favorites.",
    info2: '',
    url: '',
    repo: 'https://github.com/cobidev/react-simplefolio', // if no repo, the button will not show up
  },
  {
    id: nanoid(),
    img: 'SpaceGuardians.jpg',
    title: 'Space Guardians',
    info: '',
    info2: '',
    url: '',
    repo: 'https://github.com/cobidev/react-simplefolio', // if no repo, the button will not show up
  },
];

// CONTACT DATA
export const contactData = {
  cta: '',
  btn: '',
  email: 'prabhjotdev@live.com',
};

// FOOTER DATA
export const footerData = {
  networks: [
    {
      id: nanoid(),
      name: 'twitter',
      url: 'https://twitter.com/Prabhjotdev05',
    },
    {
      id: nanoid(),
      name: 'linkedin',
      url: 'https://www.linkedin.com/in/prabhjot-dev/',
    },
    {
      id: nanoid(),
      name: 'github',
      url: 'https://github.com/prabhjotdev',
    },
  ],
};

// Github start/fork buttons
export const githubButtons = {
  isEnabled: false, // set to false to disable the GitHub stars/fork buttons
};
